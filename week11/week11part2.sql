select * from movies ;
select * from denormalized ;

load data
infile '/home/semra/Desktop/MSKU/datamanag./denormalized.csv'
into table denormalized
columns terminated by ';' ;

show variables  like "secure_file_priv";

insert into movies(movie_id,title,ranking,rating,year,votes,duration,oscars,budget)
select distinctrow movie_id,title,ranking,rating,year,votes,duration,oscars,budget
from denormalized;

select * from movies;
##provide a table  from all of tables countries variable
insert into countries(country_id, country_name)
select distinctrow producer_country_id, producer_country_name
from denormalized
union
select distinctrow director_country_id, director_country_name
from denormalized
union
select distinctrow star_country_id, star_country_name
from denormalized
order by producer_country_id;

select * from countries; 

