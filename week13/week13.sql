Create temporary table Products_Below_avg
select ProductId, ProductName, Price
from Products
where Price < (select avg (Price) from Products)
;
drop table Products_Below_avg;
show table status;
show tables ;
select * from Products_Below_avg;