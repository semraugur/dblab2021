select * from Shippers;
Select  ShipperID, OrderID from Orders
where ShipperID in (
	select ShipperID
    from Shippers
    );
select ShipperName, count(OrderID)
from Orders join Shippers on Orders.ShipperID=Shippers.ShipperID
group by Orders.ShipperID;
#Show customer name who give one order at least
select CustomerName, OrderID
from Customers join Orders on Customers.CustomerID=Orders.CustomerID
group by OrderID;
#Show customer name who dont give order (possible see with left join)
select CustomerName, OrderID
from Customers left join Orders on Customers.CustomerID=Orders.OrderID
order by OrderID ;
#Shows employeename that prepared whole type orders
select FirstName, LastName, OrderID
from Orders join Employees on Orders.EmployeeID=Employees.EmployeeID
order by OrderID;
# shows employees names both prepared order and not prepared orders
select FirstName, LastName, OrderID
from Orders right join Employees on Orders.EmployeeID=EmployeeID
order by OrderID;


