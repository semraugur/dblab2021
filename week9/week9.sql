select * from Customers ;

update Customers set Country =replace(Country,'\n','');
update Customers set City =replace(City,'\n','');
create view MexicanCustomers as
select CustomerID , CustomerName, ContactName
from Customers
where Country ="Mexico";
select * from MexicanCustomers;

select *
from MexicanCustomers join Orders on MexicanCustomers.CustomerID=Orders.CustomerID;

select ProductID,  ProductName,  Price
from Products
Where price > avg (price);

create view Productsbelowavg as
select ProductID,  ProductName, Price
from Products
where Price < (select avg(Price) from Products) ;
#
delete from OrderDetails where ProductID=5;
#faster than delete and ıf want to use table content we use it
truncate OrderDetails;
#we need order information delete before. before we should delete relation that they have entity 
delete from Customers;
delete from Orders;

drop table Customers ;